document.addEventListener("DOMContentLoaded", () => {
  const header = document.getElementById("header");
  const navLinks = document.querySelectorAll("nav ul li a");
  const postsContainer = document.getElementById("posts");
  const paginationContainer = document.getElementById("pagination");
  const sortSelect = document.getElementById("sort");
  const itemsPerPageSelect = document.getElementById("itemsPerPage");
  const showingRange = document.getElementById("showingRange");

  const postsData = [
    { title: "Terbaru Kenali Tingkatan Influencers berdasarkan Jumlah Followers Influencers berdasarkan Jumlah Followers", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Jangan Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2022", image: "banner3.jpg" },
    { title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers berdasarkan Jumlah Followers ", date: "5 SEPTEMBER 2022", image: "banner.jpg" },
    { title: "Terlama Asal Pilih Influencer, Berikut Cara Menyusun Strategi Influencer", date: "5 SEPTEMBER 2021", image: "banner3.jpg" },
  ];

  let currentPage = parseInt(localStorage.getItem("currentPage")) || 1;
  let itemsPerPage = parseInt(localStorage.getItem("itemsPerPage")) || 8;
  let sortBy = localStorage.getItem("sortBy") || "-published_at";

  sortSelect.value = sortBy;
  itemsPerPageSelect.value = itemsPerPage;

  const setActiveLink = () => {
    const currentPage = window.location.pathname.split("/").pop();
    navLinks.forEach((link) => {
      link.classList.toggle("active", link.getAttribute("href") === currentPage);
    });
  };

  setActiveLink();

  function handleNavLinkClick(event) {
    navLinks.forEach((link) => link.classList.remove("active"));
    event.target.classList.add("active");

    const sectionId = event.target.getAttribute("href").substring(1);
    const section = document.getElementById(sectionId);

    window.scrollTo({
      top: section.offsetTop - header.offsetHeight,
      behavior: "smooth",
    });
  }

  function updateLocalStorage() {
    localStorage.setItem("sortBy", sortBy);
    localStorage.setItem("itemsPerPage", itemsPerPage);
    localStorage.setItem("currentPage", currentPage);
  }

  function sortPosts(posts, sortBy) {
    return posts.sort((a, b) => {
      const dateA = new Date(a.date);
      const dateB = new Date(b.date);
      return sortBy === "-published_at" ? dateB - dateA : dateA - dateB;
    });
  }

  function renderPosts(posts, currentPage, itemsPerPage) {
    postsContainer.innerHTML = "";
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const postsToShow = posts.slice(startIndex, endIndex);

    postsToShow.forEach((post) => {
      const postElement = document.createElement("div");
      postElement.classList.add("post");

      const imageElement = document.createElement("img");
      imageElement.setAttribute("data-src", post.image);
      imageElement.setAttribute("alt", post.title);
      imageElement.classList.add("lazy");

      const dateElement = document.createElement("div");
      dateElement.classList.add("post-date");
      dateElement.textContent = post.date;

      const titleElement = document.createElement("div");
      titleElement.classList.add("post-title");
      titleElement.textContent = post.title;

      postElement.appendChild(imageElement);
      postElement.appendChild(dateElement);
      postElement.appendChild(titleElement);

      postsContainer.appendChild(postElement);
    });

    lazyLoadImages();
    showingRange.textContent = `${startIndex + 1}-${Math.min(endIndex, posts.length)}`;
  }

  function renderPagination(totalItems, itemsPerPage, currentPage) {
    paginationContainer.innerHTML = "";
    const totalPages = Math.ceil(totalItems / itemsPerPage);

    const createButton = (text, page) => {
      const button = document.createElement("button");
      button.textContent = text;
      if (page) {
        button.addEventListener("click", () => {
          currentPage = page;
          updateLocalStorage();
          renderPosts(sortPosts(postsData, sortBy), currentPage, itemsPerPage);
          renderPagination(postsData.length, itemsPerPage, currentPage);
        });
      } else {
        button.classList.add("disabled");
      }
      return button;
    };

    paginationContainer.appendChild(createButton("«", currentPage > 1 ? currentPage - 1 : null));
    for (let i = 1; i <= totalPages; i++) {
      const button = createButton(i, i);
      if (i === currentPage) {
        button.classList.add("active");
      }
      paginationContainer.appendChild(button);
    }
    paginationContainer.appendChild(createButton("»", currentPage < totalPages ? currentPage + 1 : null));
  }

  function lazyLoadImages() {
    const lazyImages = document.querySelectorAll(".lazy");
    const lazyLoad = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            const img = entry.target;
            img.src = img.getAttribute("data-src");
            img.classList.remove("lazy");
            lazyLoad.unobserve(img);
          }
        });
      },
      {
        rootMargin: "0px 0px 50px 0px",
        threshold: 0.01,
      }
    );

    lazyImages.forEach((img) => {
      lazyLoad.observe(img);
    });
  }

  function initialize() {
    navLinks.forEach((link) => link.addEventListener("click", handleNavLinkClick));
    sortSelect.addEventListener("change", (event) => {
      sortBy = event.target.value;
      updateLocalStorage();
      currentPage = 1;
      renderPosts(sortPosts(postsData, sortBy), currentPage, itemsPerPage);
      renderPagination(postsData.length, itemsPerPage, currentPage);
    });
    itemsPerPageSelect.addEventListener("change", (event) => {
      itemsPerPage = parseInt(event.target.value);
      updateLocalStorage();

      currentPage = Math.min(currentPage, Math.ceil(postsData.length / itemsPerPage));
      renderPosts(sortPosts(postsData, sortBy), currentPage, itemsPerPage);
      renderPagination(postsData.length, itemsPerPage, currentPage);
    });

    renderPosts(sortPosts(postsData, sortBy), currentPage, itemsPerPage);
    renderPagination(postsData.length, itemsPerPage, currentPage);

    let lastScrollTop = 0;
    window.addEventListener("scroll", () => {
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      if (scrollTop > lastScrollTop) {
        header.classList.add("hidden");
      } else {
        header.classList.remove("hidden");
      }
      lastScrollTop = scrollTop;
    });

    const scrollTopButton = document.getElementById("scrollTopButton");
    scrollTopButton.addEventListener("click", () => {
      window.scrollTo({ top: 0, behavior: "smooth" });
    });

    window.addEventListener("scroll", () => {
      if (window.scrollY > window.innerHeight) {
        scrollTopButton.style.display = "block";
      } else {
        scrollTopButton.style.display = "none";
      }
    });
  }
  renderPosts(sortPosts(postsData, sortBy), currentPage, itemsPerPage);
  renderPagination(postsData.length, itemsPerPage, currentPage);

  let lastScrollY = window.scrollY;
  window.addEventListener("scroll", () => {
    if (window.scrollY > lastScrollY) {
      header.classList.add("hidden");
    } else {
      header.classList.remove("hidden");
      header.classList.add("transparent");
      setTimeout(() => {
        header.classList.remove("transparent");
      }, 300);
    }
    lastScrollY = window.scrollY;
  });

  initialize();
});
